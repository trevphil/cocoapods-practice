//
//  RWViewController.m
//  ShowTracker
//
//  Created by Joshua on 3/1/14.
//  Copyright (c) 2014 Ray Wenderlich. All rights reserved.
//

#import <AFNetworking/UIKit+AFNetworking.h>
#import <Nimbus/NIAttributedLabel.h>
#import <SAMCategories/UIScreen+SAMAdditions.h>
#import "ViewController.h"
#import "TraktAPIClient.h"

@interface ViewController () <NIAttributedLabelDelegate>

@property (strong, nonatomic) NSArray *jsonResponse;
@property (nonatomic) BOOL pageControlUsed;
@property (nonatomic) NSInteger previousPage;

@end

@implementation ViewController

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.previousPage = -1;
    
    TraktAPIClient *client = [TraktAPIClient sharedClient];
    [client getShowsForDate:[NSDate date]
                   username:@"rwtestuser"
               numberOfDays:3
                    success:^(NSURLSessionDataTask *task, id responseObject) {
                        // save response object
                        self.jsonResponse = responseObject;
                        // get the number of shows
                        NSInteger shows = 0;
                        for (NSDictionary *day in self.jsonResponse) {
                            shows += [day[@"episodes"] count];
                        }
                        // set up page control
                        self.showsPageControl.numberOfPages = shows;
                        self.showsPageControl.currentPage = 0;
                        // set up scroll view
                        self.showsScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.bounds) * shows, CGRectGetHeight(self.showsScrollView.frame));
                        // load first show
                        [self loadShow:0];
                    }
                    failure:^(NSURLSessionDataTask *task, NSError *error) {
                        NSLog(@"Failure -- %@", error);
                    }];
}

#pragma mark - Actions

- (IBAction)pageChanged:(id)sender
{
    self.pageControlUsed = YES;
    
    NSInteger page = self.showsPageControl.currentPage;
    self.previousPage = page;
    [self loadShow:page];
    
    CGRect frame = self.showsScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.showsScrollView scrollRectToVisible:frame animated:NO];
                     }
                     completion:^(BOOL finished) {
                         self.pageControlUsed = NO;
                     }];
}

- (void)loadShow:(NSInteger)index {
    // find the show for the given index
    NSDictionary *show = nil;
    NSInteger shows = 0;
    for (NSDictionary *day in self.jsonResponse) {
        NSInteger count = [day[@"episodes"] count];
        // did you find the right show?
        if (index < shows + count) {
            show = day[@"episodes"][index - shows];
            break;
        }
        // increment the shows counter
        shows += count;
    }
    
    // load the show info
    NSDictionary *showsDict = show[@"show"];
    
    // display the show title
    NIAttributedLabel *titleLabel = [[NIAttributedLabel alloc] initWithFrame:CGRectMake(index * CGRectGetWidth(self.showsScrollView.bounds), 40, CGRectGetWidth(self.showsScrollView.bounds), 40)];
    titleLabel.text = showsDict[@"title"];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.linkColor = [UIColor redColor];
    titleLabel.font = [UIFont systemFontOfSize:18.0f];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [titleLabel addLink:[NSURL URLWithString:showsDict[@"url"]]
                  range:NSMakeRange(0, titleLabel.text.length)];
    titleLabel.delegate = self;
    
    NSString *posterUrl = showsDict[@"images"][@"poster"];
    if ([[UIScreen mainScreen] sam_isRetina]) {
        posterUrl = [posterUrl stringByReplacingOccurrencesOfString:@".jpg" withString:@"-300.jpg"];
    } else {
        posterUrl = [posterUrl stringByReplacingOccurrencesOfString:@".jpg" withString:@"-138.jpg"];
    }
    
    // 5.4 - Display image using image view
    UIImageView *posterImage = [[UIImageView alloc] init];
    posterImage.frame = CGRectMake(index * CGRectGetWidth(self.showsScrollView.bounds) + 90, 105, 150, 225);
    [self.showsScrollView addSubview:posterImage];
    [posterImage setImageWithURL:[NSURL URLWithString:posterUrl] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    static NSDateFormatter *formatter = nil;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init];
        formatter.dateStyle = NSDateFormatterLongStyle;
        formatter.timeStyle = NSDateFormatterShortStyle;
        formatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"PST"];
    }
    
    NSDictionary *episodeDict = show[@"episode"];
    
    NSTimeInterval showAired = [episodeDict[@"first_aired_localized"] doubleValue];
    NSString *showDate = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970: showAired]];
    
    UILabel *episodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(index * CGRectGetWidth(self.showsScrollView.bounds),
                                                                      360, CGRectGetWidth(self.showsScrollView.bounds), 40)];
    NSString* episode  = [NSString stringWithFormat:@"%02dx%02d - \"%@\"",
                          [[episodeDict valueForKey:@"season"] intValue],
                          [[episodeDict valueForKey:@"number"] intValue],
                          [episodeDict objectForKey:@"title"]];
    episodeLabel.text = [NSString stringWithFormat:@"%@\n%@", episode, showDate];
    episodeLabel.numberOfLines = 0;
    episodeLabel.textAlignment = NSTextAlignmentCenter;
    episodeLabel.textColor = [UIColor whiteColor];
    episodeLabel.backgroundColor = [UIColor clearColor];
    
    CGSize size = [episodeLabel sizeThatFits:CGSizeMake(CGRectGetWidth(self.view.frame),
                                                        CGRectGetHeight(self.view.frame) - CGRectGetMinY(episodeLabel.frame))];
    CGRect frame = episodeLabel.frame;
    frame.size.width = self.view.frame.size.width;
    frame.size.height = size.height;
    episodeLabel.frame = frame;
    
    [self.showsScrollView addSubview:episodeLabel];
    [self.showsScrollView addSubview:titleLabel];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.pageControlUsed) return;
    
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = floor((scrollView.contentOffset.x - pageWidth/2.0) / pageWidth) + 1;
    
    // Do not do anything if we're trying to go beyond the available page range
    if (page == self.previousPage || page < 0 || page >= self.showsPageControl.numberOfPages) {
        return;
    } else {
        self.previousPage = page;
    }
    
    self.showsPageControl.currentPage = page;
    [self loadShow:page];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    self.pageControlUsed = NO;
}

#pragma mark - NIAttributedLabelDelegate

- (void)attributedLabel:(NIAttributedLabel *)attributedLabel
didSelectTextCheckingResult:(NSTextCheckingResult *)result
                atPoint:(CGPoint)point {
    [[UIApplication sharedApplication] openURL:result.URL];
}

@end
