//
//  TraktAPIClient.m
//  ShowTracker
//
//  Created by Trevor Phillips on 7/9/14.
//  Copyright (c) 2014 Ray Wenderlich. All rights reserved.
//

#import "TraktAPIClient.h"

NSString * const kTractAPIKey = @"208982b723e9b4f68c3cf98303c5f1cc";
NSString * const kTractBaseURLString = @"http://api.trakt.tv";

@implementation TraktAPIClient

+ (TraktAPIClient *)sharedClient {
    static TraktAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:kTractBaseURLString]];
    });
    return _sharedClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    return self;
}

- (void)getShowsForDate:(NSDate *)date
               username:(NSString *)username
           numberOfDays:(NSUInteger)numberOfDays
                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMdd";
    NSString *dateString = [formatter stringFromDate:date];
    
    NSString *path = [NSString stringWithFormat:@"user/calendar/shows.json/%@/%@/%@/%lu",
                      kTractAPIKey, username, dateString, (unsigned long)numberOfDays];
    [self GET:path
   parameters:nil
      success:^(NSURLSessionDataTask *task, id responseObject) {
          if (success) {
              success(task, responseObject);
          }
      } failure:^(NSURLSessionDataTask *task, NSError *error) {
          if (failure) {
              failure(task, error);
          }
      }];
}

@end
